#!/usr/bin/env bash

set -e
set -o pipefail

SOURCE_ETHERPAD='https://pad.example.com'
SOURCE_API_KEY='...' # take from /opt/etherpad-lite/APIKEY.txt
EXPORT_DIR='export'
EXPORT_LOG='export.log'
API_VERSION_URL="${SOURCE_ETHERPAD}/api/"
API_VERSION="$(curl -s -X GET ${API_VERSION_URL} | jq -r '.currentVersion')"

LIST_PADS_URL="${SOURCE_ETHERPAD}/api/${API_VERSION}/listAllPads?apikey=${SOURCE_API_KEY}"

touch "${EXPORT_LOG}"

echo "$(date --iso-8601=minutes): creating export directory '${EXPORT_DIR}'" | tee -a "${EXPORT_LOG}"
mkdir -p "${EXPORT_DIR}"

echo "$(date --iso-8601=minutes): fetching list of all pads" | tee -a "${EXPORT_LOG}"
for padName in $(curl -s -X GET "${LIST_PADS_URL}" | jq -r '.data.padIDs[]'); do
  if [ -n "$padName" ]; then
    echo "$(date --iso-8601=minutes): exporting pad '${padName}'" | tee -a "${EXPORT_LOG}"
#    status=`curl -s -w '%{http_code}' -X GET "${SOURCE_ETHERPAD}/p/${padName}/export/html" -o "${EXPORT_DIR}/${padName}.html"`
    status=`curl -s -w '%{http_code}' -X GET "${SOURCE_ETHERPAD}/${padName}/export/html" -o "${EXPORT_DIR}/${padName}.html"`
    echo "$(date --iso-8601=minutes): pad '${padName}' exported with status '${status}'" | tee -a "${EXPORT_LOG}"
  fi
done
