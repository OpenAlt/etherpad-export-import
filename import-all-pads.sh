#!/usr/bin/env bash

set -e
set -o pipefail

TARGET_ETHERPAD='https://pad.example.com'
TARGET_API_KEY='...' # take from /opt/etherpad-lite/APIKEY.txt
TARGET_PREFIX=''
EXPORT_DIR='export'
IMPORT_LOG='../import.log'
API_VERSION_URL="${TARGET_ETHERPAD}/api/"
API_VERSION="$(curl -s -X GET ${API_VERSION_URL} | jq -r '.currentVersion')"

cd "${EXPORT_DIR}"
trap 'cd -' EXIT

touch "${IMPORT_LOG}"

for file in *.html; do
  padName="${TARGET_PREFIX}${file%'.html'}"
  echo "$(date --iso-8601=minutes): importing pad '${padName}'" | tee -a "${IMPORT_LOG}"
  curl -s -o /dev/null "${TARGET_ETHERPAD}/api/${API_VERSION}/createPad?apikey=${TARGET_API_KEY}&padID=${padName}"
#  status=`curl -s -o /dev/null -w '%{http_code}' "${TARGET_ETHERPAD}/p/${padName}/import" -F 'name=file' -F "filename=${file}" -F "file=@${file}"`
  status=`curl -s -o /dev/null -w '%{http_code}' "${TARGET_ETHERPAD}/${padName}/import" -F 'name=file' -F "filename=${file}" -F "file=@${file}"`
  echo "$(date --iso-8601=minutes): pad '${padName}' imported with status '${status}'" | tee -a "${IMPORT_LOG}"
done
